INSTALL
=======

1. Extract module in /sites/all/modules/ directory
2. Enable module on /admin/modules page
3. Configure number of post and name of your widget on /admin/config/dialogfeed_widget page
4. Go to [Structure] -> [Blocks] , then you can see Dialogfeed Widget block and place it at region that you want.

CONFIGURE
=========

Visit configuration page to customize widget /admin/config/dialogfeed_widget

