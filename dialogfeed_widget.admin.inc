<?php

/**
 * @file
 * Administative form for the dialogfeed widget module.
 */

/**
 * Page callback
 * Return form with dialogfeed settings
 */
function dialogfeed_widget_admin() {
  $form = array();

  // Main settings
  $form['main_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('dialogfeed settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      );  
  $form['main_settings']['dialogfeed_widget_nbpost'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of posts:'),
      '#size' => 25,
      '#default_value' => variable_get('dialogfeed_widget_nbpost', '5'),
      '#description' => t('Number of posts displayed'),
      );

  $form['main_settings']['dialogfeed_widget_name_widget'] = array(
      '#type' => 'textfield',
      '#title' => t('Name of widget:'),
      '#size' => 25,
      '#default_value' => variable_get('dialogfeed_widget_name_widget', ''),
      '#description' => t('Name of widget'),
      );


  return system_settings_form($form);
}

function dialogfeed_widget_admin_settings_submit($form, &$form_state) {
  variable_set('dialogfeed_widget_nbpost', $form_state['values']['dialogfeed_widget_nbpost']);
  variable_set('dialogfeed_widget_name_widget', $form_state['values']['dialogfeed_widget_name_widget']);

}

/**
 * Validates dialogfeed_widget_admin form
 */
function dialogfeed_widget_admin_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (!ctype_digit($values['dialogfeed_widget_nbpost']) || $values['dialogfeed_widget_nbpost'] < 1 ) {
    form_set_error('dialogfeed_widget_nbpost', t('Please, enter only digits positif in Number of posts field'));
  }

}
